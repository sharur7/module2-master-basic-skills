# Pose Estimation Summary
* Pose estimation refers to computer vision techniques that detect human figures in images and videos, so that one could determine, for example, where someone’s elbow shows up in an image.<br>

* To be clear, this technology is not recognizing who is in an image. The algorithm is simply estimating where key body joints are.
<img src ="https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/pose_estimation.gif">

* Pose estimation has many uses, from interactive installations that react to the body to augmented reality, animation, fitness uses, and more. We hope the accessibility of this model inspires more developers and makers to experiment and apply pose detection to their own unique projects.<br>

* While many alternate pose detection systems have been open-sourced, all require specialized hardware and/or cameras, as well as quite a bit of system setup.<br>

* Great strides have been made in the field of human pose estimation, which enables us to better serve the myriad applications that are possible with it. Moreover, research in related fields such as Pose Tracking can greatly enhance its productive utilization in several fields.<br>
 
* The concepts listed in this blog are not exhaustive but rather strives to introduce some popular variants of these algorithms and their real-life applications.
