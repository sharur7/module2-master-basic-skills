# Face Recognition Summary

* In, the past few years, face recognition owned significant consideration and appreciated as one of the most promising applications in the field of image analysis. 
* Face detection can consider a substantial part of face recognition operations. According to its strength to focus computational resources on the section of an image holding a face. 
* The method of face detection in pictures is complicated because of variability present across human faces such as pose, expression, position and orientation, skin colour, the presence of glasses or facial hair, differences in camera gain, lighting conditions, and image resolution.
* Face Detection is the first and essential step for face recognition, and it is used to detect faces in the images. 
* It is a part of object detection and can use in many areas such as security, bio-metrics, law enforcement, entertainment, personal safety, etc.

<img src ="https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/face.png">

## Examples:

### RECOGNIZE VIPS AT SPORTING EVENTS
* Face recognition can be used to provide fans with a better experience. Face recognition can instantly recognize when season ticketholders attend sporting events. Event venues can offer them swag, let them skip lines and other VIP perks that result in greater season ticketholder retention.
<img src = "https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/vip.png">

### TRACK SCHOOL ATTENDANCE
* In addition to making schools safer, face recognition has the potential to track students’ attendance. Traditionally, attendance sheets can allow students to sign another student, who is ditching class, in. But China is already using face recognition to ensure students aren’t skipping class. Tablets are being used to scan students’ faces and match their photos against a database to validate their identities.
<img src = "https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/attendance.jpg">