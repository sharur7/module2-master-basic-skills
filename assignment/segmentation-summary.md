# Image Segmentation Summary

* In digital image processing and computer vision, image segmentation is the process of partitioning a digital image into multiple segments (sets of pixels, also known as image objects). 
* The goal of segmentation is to simplify and/or change the representation of an image into something that is more meaningful and easier to analyze.
* Image segmentation is typically used to locate objects and boundaries (lines, curves, etc.) in images. More precisely, image segmentation is the process of assigning a label to every pixel in an image such that pixels with the same label share certain characteristics.
<img src ="https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/img_seg.jpg">

* Within the segmentation process itself, there are two levels of granularity:

    * Semantic segmentation—classifies all the pixels of an image into meaningful classes of objects. These classes are “semantically       interpretable”    and correspond to real-world categories. For instance, you could isolate all the pixels associated with a cat and color them green.   This is also known as dense prediction because it predicts the meaning of each pixel.
    * Instance segmentation—identifies each instance of each object in an image. It differs from semantic segmentation in that it doesn’t categorize every pixel. If there are three cars in an image, semantic segmentation classifies all the cars as one instance, while instance segmentation identifies each individual car.

### Example:
The shape of the cancerous cells plays a vital role in determining the severity of the cancer. You might have put the pieces together – object detection will not be very useful here. We will only generate bounding boxes which will not help us in identifying the shape of the cells.<br>

<img src = "https://gitlab.com/sharur7/module2-master-basic-skills/-/raw/master/assignment/assets/cell.jpg">


